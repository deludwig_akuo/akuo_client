import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {ProjectsService} from '../../../shared/services/projects/projects.service';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
// import {ObligationPopupComponent} from '../../com/obligation-popup/obligation-popup.component';
import {OperationsLogService} from '../../../shared/services/operations-log.service';
import {AddOperationComponent} from '../add-operation/add-operation.component';
import {Subscription} from 'rxjs';
import {DatetimeHelperService} from '../../../shared/services/datetime-helper.service';

@Component({
  selector: 'app-operator-list',
  templateUrl: './operator-list.component.html',
  styleUrls: ['./operator-list.component.scss']
})
export class OperatorListComponent implements OnInit, OnDestroy {

  public isPending: boolean;
  public items_filtered: any[];

  // variables to hold message subscriptions so that we can unsubscribe on OnDestroy
  private logsUpdated: Subscription;
  // private projectsUpdated: Subscription;

  private dateTimeHelper = new DatetimeHelperService();

  constructor(
      public operatorLogsService: OperationsLogService,
      private cdr: ChangeDetectorRef,
      private dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.isPending = true;
    this.logsUpdated = this.operatorLogsService.logUpdated.subscribe( data => {this.processLogsUpdated(); });
    // this.projectsUpdated = this.operatorLogsService.projectsUpdated.subscribe( data => {this.processProjectsUpdated();});
    this.operatorLogsService.getOperationsLogs();
    // this.operatorLogsService.getProjects();
  }

  public showAddOperation() {

    const dt = this.dateTimeHelper.convertFromLocalAsStringToFormControlString(new Date().toLocaleString());
    const dialogRef: MatDialogRef<any> = this.dialog.open(AddOperationComponent, {
      width: '300px',
      disableClose: true,
      data: {payload: {timestamp: dt, invoiceable: false, projectId: 1}}
      // projectName: 'test', projectId: 1
    });
    dialogRef.afterClosed().subscribe( res => {this.processAddOperationPopup(res); } );

  }

  private processAddOperationPopup(res) {
    if (!res) {return; }
    this.isPending = true;
    this.operatorLogsService.addOperation(res);
  }

  private processLogsUpdated() {
    this.items_filtered = this.operatorLogsService.operationsLogs;
    this.isPending = false;
    this.cdr.markForCheck();
  }

  private processProjectsUpdated() {
    const x = 7;
  }

  ngOnDestroy(): void {

  }

}
