import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Observable} from "rxjs";
import {OperationsLogService} from "../../../shared/services/operations-log.service";


@Component({
  selector: 'app-add-operation',
  templateUrl: './add-operation.component.html',
  styleUrls: ['./add-operation.component.scss']
})
export class AddOperationComponent implements OnInit {

  public myForm: FormGroup;
  public projects: any[];

  constructor(
      @Inject(MAT_DIALOG_DATA) public data: any,
      public dialogRef: MatDialogRef<AddOperationComponent>,
      private fb: FormBuilder,
      public operatorLogsService: OperationsLogService,
  ) {
    // const obs: Observable<any>[] = Array();
    // obs.push(this.operatorLogsService.getProjects_api());
    this.operatorLogsService.getProjects_api().subscribe( data => {
      this.projects = data;
    });
  }

  ngOnInit(): void {
    this.buildForm(this.data.payload);

  }

  buildForm(item) {
    this.myForm = this.fb.group ({
      projectName: [item.projectName || null],
      projectId: [item.projectId || null],
      timestamp: [item.timestamp || new Date()],
      equipmentName: [item.equipmentName || null],
      type: [item.type || null],
      invoiceable: [item.invoiceable || false],
      mal: [item.mal || null],
      breakerClosed: [item.closedBreaker || null],
      breakerName: [item.breakerName || null],
      notes: [item.notes || null],
      description: [item.description || null],
    });
  }

  submit(form) {
    const merged: object = {...this.data.payload, ...this.myForm.value};
    this.dialogRef.close(merged);
  }

}
