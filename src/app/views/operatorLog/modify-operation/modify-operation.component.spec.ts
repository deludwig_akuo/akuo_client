import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyOperationComponent } from './modify-operation.component';

describe('ModifyOperationComponent', () => {
  let component: ModifyOperationComponent;
  let fixture: ComponentFixture<ModifyOperationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModifyOperationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyOperationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
