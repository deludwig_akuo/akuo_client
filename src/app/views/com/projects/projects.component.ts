import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {ProjectsService} from '../../../shared/services/projects/projects.service';
import {Subscription} from 'rxjs';
// import {DomainService} from '../../../shared/services/domain/domain.service';
import {Router} from '@angular/router';
@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit, OnDestroy {

  public isPending: boolean;
  public items_filtered: any[];

  // variables to hold message subscriptions so that we can unsuscribe on OnDestroy
  private selectedProjectChanged: Subscription;

  constructor(
      private projectService: ProjectsService,
      private cdr: ChangeDetectorRef,
      private router: Router,
  ) { }

  ngOnInit(): void {

    this.selectedProjectChanged = this.projectService.selectedProjectChanged.subscribe( data => {this.projectChanged() } );

    // Get project data from the service
    this.projectService.getProjects();
    // });
  }



  public showAddProject() {}

  public viewObligations(id) {
    this.projectService.updateSelectedProject(id);
    this.router.navigate(['dashboard/obligations']);
}

  private projectChanged() {
    this.items_filtered = this.projectService.projects;
  }

  ngOnDestroy(): void {
    this.selectedProjectChanged.unsubscribe();
  }


}
