import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-schedule-popup',
  templateUrl: './schedule-popup.component.html',
  styleUrls: ['./schedule-popup.component.scss']
})
export class SchedulePopupComponent implements OnInit {

  public myForm: FormGroup;
  public responsible: '';
  public recipients: '';
  public description: '';
  public catagory: '';
  public contractName: '';
  public notes: '';
  public dueDate: '';
  public projectname: '';


  constructor(
      @Inject(MAT_DIALOG_DATA) public data: any,
      public dialogRef: MatDialogRef<SchedulePopupComponent>,
      private fb: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.responsible = this.data.payload.responsible;
    this.recipients = this.data.payload.recipients;
    this.description = this.data.payload.description;
    this.catagory = this.data.payload.catagory;
    this.contractName = this.data.payload.contract;
    this.notes = this.data.payload.notes;
    this.dueDate = this.data.payload.due;
    this.projectname = this.data.payload.project;

    this.buildForm(this.data.payload);
  }

  buildForm(item) {
    this.myForm = this.fb.group({
      isComplete: [item.isComplete || false ],
      hoursToComplete: [item.hoursToComplete || 0 ],
    });
  }

  submit(form) {
    // const x = 7;
    const merged: object = {...this.data.payload, ...this.myForm.value};



    this.dialogRef.close(merged);
  }

}
