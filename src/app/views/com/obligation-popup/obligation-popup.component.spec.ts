import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ObligationPopupComponent } from './obligation-popup.component';

describe('ObligationPopupComponent', () => {
  let component: ObligationPopupComponent;
  let fixture: ComponentFixture<ObligationPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ObligationPopupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ObligationPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
