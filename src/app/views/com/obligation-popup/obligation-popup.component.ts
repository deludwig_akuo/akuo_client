import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-obligation-popup',
  templateUrl: './obligation-popup.component.html',
  styleUrls: ['./obligation-popup.component.scss']
})
export class ObligationPopupComponent implements OnInit {

  public myForm: FormGroup;
  public title: 'Waiting for update';

  constructor(
      @Inject(MAT_DIALOG_DATA) public data: any,
      public dialogRef: MatDialogRef<ObligationPopupComponent>,
      private fb: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.title = this.data.title;
    this.buildForm(this.data);
  }

  buildForm(item) {
    this.myForm = this.fb.group( {
      description: [item.description || null],
      enactDate: [item.enactDate || null],
      startMonth: [item.startMonth || null],
      frequency: [item.frequency || null],
      contractName: [item.contractName || null],
      recipients: [item.recipients || null],
      projectId: [item.projectId || null],
      notes: [item.notes || null],
      enabled: [item.enabled || null],
      catagory: [item.catagory || null],
      delay: [item.delay || null],
      responsible: [item.responsible || null]
    });
  }

  submit(form) {
    const merged: object = {...this.data.payload, ...this.myForm.value};
    this.dialogRef.close(merged);
  }

}
