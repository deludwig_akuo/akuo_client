import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import { ProjectsService} from '../../../shared/services/projects/projects.service';
import {Subscription} from 'rxjs';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {ObligationPopupComponent} from '../obligation-popup/obligation-popup.component';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-obligations',
  templateUrl: './obligations.component.html',
  styleUrls: ['./obligations.component.scss']
})
export class ObligationsComponent implements OnInit, OnDestroy {

  myForm: FormGroup;
  public isPending: boolean;
  public items: any[];
  public items_filtered: any[];

  // variables to hold message subscriptions so that we can unsubscribe on OnDestroy
  private selectedProjectChanged: Subscription;

  constructor(
      public projectService: ProjectsService,
      private cdr: ChangeDetectorRef,
      private dialog: MatDialog,
      private fb: FormBuilder
  ) {

    this.myForm = this.fb.group({filterText: ['']});

  }

  ngOnInit(): void {
    this.selectedProjectChanged = this.projectService.selectedProjectChanged.subscribe( data => {this.projectChanged()});
    this.items = this.projectService.selectedProject['obligations'];
    this.items_filtered = this.items;
    // this.projectService.getObligations_api().subscribe( data => {
    //   this.items_filtered = data;
    // });
  }

  private projectChanged() {
    this.items = this.projectService.selectedProject['obligations'];
  }

  updateFilter(event) {
    this.items_filtered = this.items.slice();
    const val = event.target.value.toLowerCase();
    let items_filter = this.myForm.controls['filterText'].value;

    if (items_filter === '') {
      return true;
    }
    items_filter = items_filter.toLowerCase();
    let columns = Object.keys(this.items_filtered[0]);

    if (!columns.length) {
      return false;
    }

    this.items_filtered = this.items_filtered.filter(function(row) {
      for (let i = 0; i <= columns.length; i++) {
        if (row[columns[i]] && row[columns[i]].toString().toLowerCase().indexOf(items_filter) > -1) {
          return true;
        }
      }
      return false;
    });

    this.cdr.markForCheck();
  }


  public showAddObligation(dataIn: any={}, isNewItem?) {
    let titleStr = '';
    if (!isNewItem) {
      titleStr = 'Edit Obligation';
    } else {
      titleStr = 'Add Obligation';
    }

    const dialogRef: MatDialogRef<any> = this.dialog.open(ObligationPopupComponent, {
      width: '500px',
      disableClose: true,
      data:  {title: titleStr,
              isNew: isNewItem,
              description: dataIn.description,
              enactDate: dataIn.enactDate,
              startMonth: dataIn.startMonth,
              contractName: dataIn.contractName,
              recipients: dataIn.recipients,
              projectId: this.projectService.selectedProject['projectId'],
              notes: dataIn.notes,
              enabled: dataIn.enabled,
              catagory: dataIn.catagory,
              delay: dataIn.delay,
              responsible: dataIn.responsible,
              frequency: dataIn.frequency
      }

    });
    dialogRef.afterClosed().subscribe( res => {this.processObligationPopup(res); } );
  }

  private processObligationPopup(res) {
    if (!res) { return; }
    this.projectService.addObligation(res);
  }

  ngOnDestroy(): void {
    this.selectedProjectChanged.unsubscribe();
  }

}
