import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {ProjectsService} from '../../../shared/services/projects/projects.service';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {SchedulePopupComponent} from '../schedule-popup/schedule-popup.component';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss']
})
export class ScheduleComponent implements OnInit, OnDestroy {

  myForm: FormGroup;
  public isPending: boolean;
  public items: any[];
  public items_filtered: any[];

  // Variables to hold message subscriptions so that we can unsubscribe on OnDestroy
  private selectedProjectChanged: Subscription;
  private deliverablesUpdated: Subscription;

  constructor(
      public projectService: ProjectsService,
      private cdr: ChangeDetectorRef,
      private dialog: MatDialog,
      private fb: FormBuilder
  ) {
    this.myForm = this.fb.group({filterText: ['']});
  }

  ngOnInit(): void {
    this.isPending = true;
    this.selectedProjectChanged = this.projectService.selectedProjectChanged.subscribe( data => {this.projectChanged();});
    this.deliverablesUpdated = this.projectService.deliverablesUpdated.subscribe( data => {this.processDeliverablesUpdated();});
    // this.items = this.projectService.deliverables;
    // this.items_filtered = this.projectService.deliverables;
    this.projectService.getDeliverables();
    // this.projectService.computeDeliverables();
  }

  public computeSchedule() {
    this.isPending = true;
    this.cdr.markForCheck();
    this.projectService.computeDeliverables();
  }

  public editDeliverable(row) {

    const dialogRef: MatDialogRef<any> = this.dialog.open(SchedulePopupComponent, {
      width: '500px',
      disableClose: true,
      data: {payload: row}
    });
    dialogRef.afterClosed().subscribe(res => {this.processEditDeliverable(res); } );
  }

  private processEditDeliverable(res) {
    if (!res) { return; }

    this.projectService.updateDeliverable(res);
    this.isPending = true;
    this.cdr.markForCheck();
  }

  private projectChanged() {
    this.items_filtered = this.projectService.selectedProject['deliverables'];
    this.items = this.items_filtered;
    this.isPending = false;
    this.cdr.markForCheck();
  }

  private processDeliverablesUpdated() {
    this.items_filtered = this.projectService.deliverables;
    this.items = this.items_filtered;
    this.isPending = false;
    this.cdr.markForCheck();
  }

  ngOnDestroy(): void {
    this.selectedProjectChanged.unsubscribe();
    this.deliverablesUpdated.unsubscribe();
  }

  updateFilter(event) {
    this.items_filtered = this.items.slice();
    let items_filter = this.myForm.controls['filterText'].value;
    if (items_filter === '') {
      return true;
    }
    items_filter = items_filter.toLowerCase();
    const columns = Object.keys(this.items_filtered[0]);

    if (!columns.length) {
      return false;
    }

    this.items_filtered = this.items_filtered.filter(function(row) {
      for (let i = 0; i <= columns.length; i++) {
        if (row[columns[i]] && row[columns[i]].toString().toLowerCase().indexOf(items_filter) > -1) {
          return true;
        }
      }
      return false;
    });
    this.cdr.markForCheck();
  }
}
