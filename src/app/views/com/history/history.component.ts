import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {ProjectsService} from '../../../shared/services/projects/projects.service';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit, OnDestroy {

  public isPending: boolean;
  public items_filtered: any[];

  // Variables to hold message subscriptions so that we can unsubscribe on OnDestroy
  // private selectedProjectChanged: Subscription;
  // private deliverablesUpdated: Subscription;
  private historyUpdated: Subscription;

  constructor(
      public projectService: ProjectsService,
      private cdr: ChangeDetectorRef,
      private dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.isPending = true;
    this.cdr.markForCheck();
    this.historyUpdated = this.projectService.historyUpdated.subscribe( data => {this.processUpdatedHistory(); } ) ;
    // this.items_filtered = this.projectService.deliverables;
    this.projectService.getCompletedDeliverables();
  }

  // public computeSchedule() {
  //   this.projectService.computeDeliverables();
  // }

  private processUpdatedHistory() {
    this.items_filtered = this.projectService.history;
    this.isPending = false;
    this.cdr.markForCheck();
  }

  ngOnDestroy(): void {
    this.historyUpdated.unsubscribe();
  }

}
