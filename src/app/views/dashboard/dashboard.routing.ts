import { Routes } from '@angular/router';

import { AnalyticsComponent } from './analytics/analytics.component';
import { DashboardDarkComponent } from './dashboard-dark/dashboard-dark.component';
import { CryptocurrencyComponent } from './cryptocurrency/cryptocurrency.component';
import { DefaultDashboardComponent } from './default-dashboard/default-dashboard.component';
import { UserRoleGuard } from 'app/shared/guards/user-role.guard';
import { LearningManagementComponent } from './learning-management/learning-management.component';
import { AnalyticsAltComponent } from './analytics-alt/analytics-alt.component';
import {ProjectsComponent} from '../com/projects/projects.component';
import {ObligationsComponent} from '../com/obligations/obligations.component';
import {ScheduleComponent} from '../com/schedule/schedule.component';
import {HistoryComponent} from '../com/history/history.component';
import { config } from 'config';
import {OperatorListComponent} from '../operatorLog/operator-list/operator-list.component';
import {AddOperationComponent} from '../operatorLog/add-operation/add-operation.component';


export const DashboardRoutes: Routes = [
  {
    path: 'default',
    component: DefaultDashboardComponent,
    canActivate: [UserRoleGuard],
    data: { title: 'Default', breadcrumb: 'Default', roles: config.authRoles.sa }
  },
  {
    path: 'projects',
    component: ProjectsComponent,
    data: { title: 'Projects', breadcrumb: 'PROJECTS'}
  },
  {
    path: 'obligations',
    component: ObligationsComponent,
    data: {title: 'Obligations', breadcrumb: 'OBLIGATIONS'}
  },
  {
    path: 'schedule',
    component: ScheduleComponent,
    data: {title: 'Schedule', breadcrumb: 'SCHEDULE'}
  },
  {
    path: 'history',
    component: HistoryComponent,
    data: {title: 'History', breadcrumb: 'History'}
  },
  {
    path: 'operations',
    component: OperatorListComponent,
    data: {titie: 'Operator Logs', breadcrumb: 'Operator Logs'}
  },
  {
    path: 'addoperation',
    component: AddOperationComponent,
    data: {title: 'Add Operation', breadcrumb: 'Add Operation'}
  }
  // {
  //   path: 'learning-management',
  //   component: LearningManagementComponent,
  //   data: { title: 'Learning management', breadcrumb: 'Learning management' }
  // },
  // {
  //   path: 'analytics',
  //   component: AnalyticsComponent,
  //   data: { title: 'Analytics', breadcrumb: 'Analytics' }
  // },
  // {
  //   path: 'analytics-alt',
  //   component: AnalyticsAltComponent,
  //   data: { title: 'Analytics Alternative', breadcrumb: 'Analytics Alternative' }
  // },
  // {
  //   path: 'crypto',
  //   component: CryptocurrencyComponent,
  //   data: { title: 'Cryptocurrency', breadcrumb: 'Cryptocurrency' }
  // },
  // {
  //   path: 'dark',
  //   component: DashboardDarkComponent,
  //   data: { title: 'Dark Cards', breadcrumb: 'Dark Cards' }
  // }
];
