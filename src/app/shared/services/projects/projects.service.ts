import { Injectable } from '@angular/core';
import {DomainService} from '../domain/domain.service';
import {Observable, Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ProjectsService {

  private baseURL: string;
  public selectedProject;
  public projects: any[];
  public deliverables: any[];
  public history: any[];

  selectedProjectChanged = new Subject<any>();
  deliverablesUpdated = new Subject<any>();
  historyUpdated = new Subject<any>();


  constructor(
      private http: HttpClient,
      public domainService: DomainService,
  ) {
    this.baseURL = domainService.getBaseURL();
  }

  public updateSelectedProject(id) {
    for (const prj of this.projects) {
      if (prj['id'] === id) {
        this.selectedProject = prj;
        break;
      }
    }
  }

  public computeDeliverables() {
    this.computeDeliverables_api().subscribe( data => {
      this.deliverables = data;
      this.deliverablesUpdated.next();
    });
  }

  public getDeliverables() {
    this.getDeliverables_api().subscribe( data => {
      this.deliverables = data;
      this.deliverablesUpdated.next();
    });
  }

  private getDeliverables_api(): Observable<any> {
    return this.http.get(this.baseURL + 'com/getopendeliverables');
  }

  public getProjects() {
    this.getProjects_api().subscribe( data => {
      this.projects = data;
      this.selectedProjectChanged.next();
    });
  }

  public updateDeliverable(data: any) {
    this.updateDeliverable_api(data).subscribe( dataRet => {
      this.deliverables = dataRet;
      this.deliverablesUpdated.next();
    });
  }

  public getCompletedDeliverables() {
    this.getCompletedDeliverables_api().subscribe(data => {
      this.history = data;
      this.historyUpdated.next();
    });
  }

  public addObligation(data) {
    this.addObligation_api(data).subscribe( ret => {
      this.projects = data;
    });
  }

  private addObligation_api(data): Observable<any> {
    return this.http.post(this.baseURL + 'com/addObligation', data);
  }

  private getProjects_api(): Observable<any> {
    return this.http.get(this.baseURL + 'com/getprojects');
  }

  private computeDeliverables_api(): Observable<any> {
    return this.http.get(this.baseURL + 'com/calculatedeliverables');
  }

  private updateDeliverable_api(data: any[]): Observable<any> {
    return this.http.post(this.baseURL + 'com/updatedeliverables', data);
  }

  private getCompletedDeliverables_api(): Observable<any> {
    return this.http.get(this.baseURL + 'com/getcompleteddeliverables');
  }

  // public getObligations_api(): Observable<any> {
  //   return this.http.get(this.baseURL + 'com/getobligations');
  // }
}
