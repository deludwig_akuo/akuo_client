import { Injectable } from '@angular/core';
// import { JwtHelperService } from '@auth0/angular-jwt';
import {HttpClient} from '@angular/common/http';
import {DomainService} from './domain/domain.service';

@Injectable({
  providedIn: 'root'
})
export class UnAuthService {
  baseURL: string;


  constructor(private http: HttpClient, public domainService: DomainService) {
    this.baseURL = domainService.getBaseURL();
  }

  login(model: any) {
    return this.http.post(this.baseURL + 'login', model);
  }

  logout() {
    const model: object = Object();
    return this.http.post(this.baseURL + 'logout', model);
  }

  register(model: any) {
    return this.http.post(this.baseURL + 'sessions/register', model);
  }

  forgotPassword(model: any) {
    return this.http.post(this.baseURL + 'accountreset', model).subscribe({
      next(any) {
        location.href = '/sessions/forgot-password-email';
      },
      error(msg) {console.log('Error: ', msg); }
    });
  }

}
