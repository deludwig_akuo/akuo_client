import { Injectable } from '@angular/core';
import {DomainService} from './domain/domain.service';
import {Observable, Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class OperationsLogService {

  private baseURL: string;
  public operationsLogs: any[];
  public projects: any[];

  logUpdated = new Subject<any>();
  projectsUpdated = new Subject<any>();

  constructor(
      private http: HttpClient,
      public domainService: DomainService,
  ) {
    this.baseURL = domainService.getBaseURL();
  }

  public getOperationsLogs() {
    this.getOperationsLogs_api().subscribe(data => {
      this.operationsLogs = data;
      this.logUpdated.next();
    });
  }

  public addOperation(data) {
    this.addOperation_api(data).subscribe( ret => {
      this.operationsLogs = ret;
      this.logUpdated.next();
    });
  }

  public getProjects() {
    this.getProjects_api().subscribe( ret => {
      this.projects = ret;
      this.projectsUpdated.next();
    });
  }

  private getOperationsLogs_api(): Observable<any> {
    return this.http.get(this.baseURL + 'operatorlog/getoperatorlogs');
  }

  private addOperation_api(data: object): Observable<any> {
    return this.http.post( this.baseURL + 'operatorlog/addoperatorlog', data);
  }

  public getProjects_api(): Observable<any> {
    return this.http.get(this.baseURL + 'operatorlog/getprojects');
  }


}
