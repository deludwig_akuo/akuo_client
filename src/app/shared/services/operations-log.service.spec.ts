import { TestBed } from '@angular/core/testing';

import { OperationsLogService } from './operations-log.service';

describe('OperationsLogService', () => {
  let service: OperationsLogService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OperationsLogService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
