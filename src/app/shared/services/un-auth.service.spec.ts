import { TestBed } from '@angular/core/testing';

import { UnAuthService } from './un-auth.service';

describe('UnAuthService', () => {
  let service: UnAuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UnAuthService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
