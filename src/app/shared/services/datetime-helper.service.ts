import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DatetimeHelperService {

  constructor() { }

  convertFromLocalAsStringToFormControlString(timeToConvert: string) {
    if (timeToConvert === '') { return ''; }
    const ttc = new Date(timeToConvert);
    const month = ttc.getMonth() + 1;
    let monthStr = '';
    if (month < 10) {
      monthStr = '0' + month;
    } else {
      monthStr = month.toString();
    }

    const day = ttc.getDate();
    let dayStr = '';
    if (day < 10) {
      dayStr = '0' + day;
    } else {
      dayStr = day.toString();
    }

    const hr = ttc.getHours();
    let hrStr = '';
    if (hr < 10) {
      hrStr = '0' + hr.toString();
    } else {
      hrStr = hr.toString();
    }

    const min = ttc.getMinutes();
    let minStr = '';
    if (min < 10) {
      minStr = '0' + min;
    } else {
      minStr = min.toString();
    }

    const sec = ttc.getSeconds();
    let secStr = '';
    if (sec < 10) {
      secStr = '0' + sec.toString();
    } else {
      secStr = sec.toString();
    }

    const retVal = ttc.getFullYear() + '-' + monthStr + '-' + dayStr + 'T' + hrStr + ':' + minStr + ':' + secStr;
    return retVal;
  }

}
