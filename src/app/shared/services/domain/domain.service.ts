import {Injectable, isDevMode} from '@angular/core';
import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DomainService {
  prodHostURL = '';
  environment = environment;
  localHostURL = 'http://localhost:5000/';

  constructor() { }

  getBaseURL() {
    if (isDevMode()) {
      return this.localHostURL;
    } else {
      // if (this.environment.branch === 'test') {
      //   return '';
      // } else { return '';}
      return this.localHostURL;
    }
  }
}
